// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRIDHIERITERATOR_HH
#define DUNE_P4ESTGRIDHIERITERATOR_HH

#include "p4estgridentitypointer.hh"
/** \file
 * \brief The P4estGridHierarchicIterator class
 */

namespace Dune {


  //**********************************************************************
  //
  /** \brief Iterator over the descendants of an entity.
   * \ingroup P4estGrid
     Mesh entities of codimension 0 ("elements") allow to visit all entities of
     codimension 0 obtained through nested, hierarchic refinement of the entity.
     Iteration over this set of entities is provided by the HierarchicIterator,
     starting from a given entity.
   */
  template<class GridImp>
  class P4estGridHierarchicIterator :
    public Dune::P4estGridEntityPointer<0,GridImp,typename GridImp::HostGridType::template Codim<0>::Entity::HierarchicIterator>
  {

    // Type of the corresponding HierarchicIterator in the host grid
    typedef typename GridImp::HostGridType::template Codim<0>::Entity::HierarchicIterator HostGridHierarchicIterator;

    typedef Dune::P4estGridEntityPointer<0,GridImp,typename GridImp::HostGridType::template Codim<0>::Entity::HierarchicIterator> Base;

  public:

    typedef typename Base::Entity Entity;

    //! the default Constructor
    explicit P4estGridHierarchicIterator(const GridImp* p4estgrid, const Entity& startEntity, int maxLevel) :
      Base(p4estgrid, GridImp::getRealImplementation(startEntity).hostEntity_.hbegin(maxLevel))
    {}


    //! \todo Please doc me !
    explicit P4estGridHierarchicIterator(const GridImp* p4estgrid, const Entity& startEntity, int maxLevel, bool endDummy) :
      Base(p4estgrid, GridImp::getRealImplementation(startEntity).hostEntity_.hend(maxLevel))
    {}


    //! \todo Please doc me !
    void increment()
    {
      ++this->hostEntityPointer_;
    }

  };


}  // end namespace Dune

#endif
