// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRID_ENTITY_POINTER_HH
#define DUNE_P4ESTGRID_ENTITY_POINTER_HH

#include "p4estgridentity.hh"

/** \file
 * \brief The P4estGridEntityPointer class
 */

namespace Dune {


  /** Acts as a pointer to an  entities of a given codimension.
   */
  template<int codim, class GridImp, class HostGridEntityPointer_>
  class P4estGridEntityPointer
  {
  private:

    enum { dim = GridImp::dimension };

    template<int, typename, typename>
    friend class P4estGridEntityPointer;


  public:

    //! export the type of the EntityPointer Implementation.
    //! Necessary for the typeconversion between Iterators and EntityPointer
    typedef P4estGridEntityPointer EntityPointerImp;

    /** \brief Codimension of entity pointed to */
    enum { codimension = codim };

    typedef typename GridImp::template Codim<codim>::Entity Entity;

    // The codimension of this entitypointer wrt the host grid
    enum {CodimInHostGrid = GridImp::HostGridType::dimension - GridImp::dimension + codim};

    // EntityPointer to the equivalent entity in the host grid
    typedef HostGridEntityPointer_ HostGridEntityPointer;


    //! constructor
    P4estGridEntityPointer (const GridImp* p4estgrid, const HostGridEntityPointer& hostEntityPointer)
      : p4estgrid_(p4estgrid)
      , hostEntityPointer_(hostEntityPointer)
    {}

    ///! copy constructor from EntityPointer storing different host EntityPointer
    template<typename ForeignHostGridEntityPointer>
    explicit P4estGridEntityPointer (const P4estGridEntityPointer<codim,GridImp,ForeignHostGridEntityPointer>& entityPointer)
      : p4estgrid_(entityPointer.p4estgrid_)
      , hostEntityPointer_(entityPointer.hostEntityPointer_)
    {}

    ///! assignment operator from EntityPointer storing different host EntityPointer
    template<typename ForeignHostGridEntityPointer>
    P4estGridEntityPointer& operator=(const P4estGridEntityPointer<codim,GridImp,ForeignHostGridEntityPointer>& entityPointer)
    {
      hostEntityPointer_ = entityPointer.hostEntityPointer_;
      return *this;
    }

    //! Move constructor to avoid copying the host EntityPointer
    P4estGridEntityPointer (const GridImp* p4estgrid, HostGridEntityPointer&& hostEntityPointer)
      : p4estgrid_(p4estgrid)
      , hostEntityPointer_(std::move(hostEntityPointer))
    {}

    //! Constructor from an P4estGrid entity
    P4estGridEntityPointer (const P4estGridEntity<codim,dim,GridImp>& entity)
      : p4estgrid_(entity.p4estgrid_)
      , hostEntityPointer_(entity.hostEntity_)
    {}

    //! equality
    bool equals(const P4estGridEntityPointer& i) const {
      return hostEntityPointer_ == i.hostEntityPointer_;
    }

    //! equality with EntityPointer based on different host EntityPointer
    template<typename ForeignHostGridEntityPointer>
    bool equals(const P4estGridEntityPointer<codim,GridImp,ForeignHostGridEntityPointer>& entityPointer) const
    {
      return  hostEntityPointer_ == entityPointer.hostEntityPointer_;
    }

    //! dereferencing
    Entity dereference() const {
      return Entity{{p4estgrid_,*hostEntityPointer_}};
    }

    //! Make this pointer as small as possible
    void compactify () {
      //virtualEntity_.getTarget().compactify();
    }

    //! ask for level of entity
    int level () const {
      return hostEntityPointer_->level();
    }


  protected:

    const GridImp* p4estgrid_;

    //! host EntityPointer
    HostGridEntityPointer hostEntityPointer_;


  };


} // end namespace Dune

#endif
