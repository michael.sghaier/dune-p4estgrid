// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRIDENTITY_HH
#define DUNE_P4ESTGRIDENTITY_HH

/** \file
 * \brief The P4estGridEntity class
 */

#include <dune/grid/common/grid.hh>

namespace Dune {


  // Forward declarations

  template<int codim, int dim, class GridImp>
  class P4estGridEntity;

  template<int codim, class GridImp, typename HostEntityPointer>
  class P4estGridEntityPointer;

  template<int codim, PartitionIteratorType pitype, class GridImp>
  class P4estGridLevelIterator;

  template<class GridImp>
  class P4estGridLevelIntersectionIterator;

  template<class GridImp>
  class P4estGridLeafIntersectionIterator;

  template<class GridImp>
  class P4estGridHierarchicIterator;


  // External forward declarations
  template< class Grid >
  struct HostGridAccess;


  //**********************************************************************
  //
  // --P4estGridEntity
  // --Entity
  //
  /** \brief The implementation of entities in a P4estGrid
   *   \ingroup P4estGrid
   *
   *  A Grid is a container of grid entities. An entity is parametrized by the codimension.
   *  An entity of codimension c in dimension d is a d-c dimensional object.
   *
   */
  template<int codim, int dim, class GridImp>
  class P4estGridEntity :
    public EntityDefaultImplementation <codim,dim,GridImp,P4estGridEntity>
  {

    template <class GridImp_>
    friend class P4estGridLevelIndexSet;

    template <class GridImp_>
    friend class P4estGridLeafIndexSet;

    template <class GridImp_>
    friend class P4estGridLocalIdSet;

    template <class GridImp_>
    friend class P4estGridGlobalIdSet;

    template<int,typename, typename>
    friend class P4estGridEntityPointer;

    friend struct HostGridAccess< typename remove_const< GridImp >::type >;


  private:

    typedef typename GridImp::ctype ctype;

    // The codimension of this entitypointer wrt the host grid
    enum {CodimInHostGrid = GridImp::HostGridType::dimension - GridImp::dimension + codim};

    // EntityPointer to the equivalent entity in the host grid
    typedef typename GridImp::HostGridType::Traits::template Codim<CodimInHostGrid>::Entity HostGridEntity;


  public:

    typedef typename GridImp::template Codim<codim>::Geometry Geometry;

    //! The type of the EntitySeed interface class
    typedef typename GridImp::template Codim<codim>::EntitySeed EntitySeed;

    P4estGridEntity()
      : p4estgrid_(nullptr)
    {}

    P4estGridEntity(const GridImp* p4estgrid, const HostGridEntity& hostEntity)
      : hostEntity_(hostEntity)
      , p4estgrid_(p4estgrid)
    {}

    P4estGridEntity(const GridImp* p4estgrid, HostGridEntity&& hostEntity)
      : hostEntity_(std::move(hostEntity))
      , p4estgrid_(p4estgrid)
    {}

    //! \todo Please doc me !
    P4estGridEntity(const P4estGridEntity& original)
      : hostEntity_(original.hostEntity_)
      , p4estgrid_(original.p4estgrid_)
    {}

    P4estGridEntity(P4estGridEntity&& original)
      : hostEntity_(std::move(original.hostEntity_))
      , p4estgrid_(original.p4estgrid_)
    {}

    //! \todo Please doc me !
    P4estGridEntity& operator=(const P4estGridEntity& original)
    {
      if (this != &original)
      {
        p4estgrid_ = original.p4estgrid_;
        hostEntity_ = original.hostEntity_;
      }
      return *this;
    }

    //! \todo Please doc me !
    P4estGridEntity& operator=(P4estGridEntity&& original)
    {
      if (this != &original)
      {
        p4estgrid_ = original.p4estgrid_;
        hostEntity_ = std::move(original.hostEntity_);
      }
      return *this;
    }

    bool equals(const P4estGridEntity& other) const
    {
      return hostEntity_ == other.hostEntity_;
    }

    //! returns true if father entity exists
    bool hasFather () const {
      return hostEntity_.hasFather();
    }

    //! Create EntitySeed
    EntitySeed seed () const
    {
      return EntitySeed(hostEntity_);
    }

    //! level of this element
    int level () const {
      return hostEntity_.level();
    }


    /** \brief The partition type for parallel computing
     */
    PartitionType partitionType () const {
      return hostEntity_.partitionType();
    }


    /** Intra-element access to entities of codimension cc > codim. Return number of entities
     * with codimension cc.
     */
    template<int cc> int count () const {
      return hostEntity_.template count<cc>();
    }


    //! geometry of this entity
    Geometry geometry () const
    {
      return Geometry( hostEntity_.geometry() );
    }


    HostGridEntity hostEntity_;

  private:

    const GridImp* p4estgrid_;

  };




  //***********************
  //
  //  --P4estGridEntity
  //
  //***********************
  /** \brief Specialization for codim-0-entities.
   * \ingroup P4estGrid
   *
   * This class embodies the topological parts of elements of the grid.
   * It has an extended interface compared to the general entity class.
   * For example, Entities of codimension 0  allow to visit all neighbors.
   */
  template<int dim, class GridImp>
  class P4estGridEntity<0,dim,GridImp> :
    public EntityDefaultImplementation<0,dim,GridImp, P4estGridEntity>
  {
    friend struct HostGridAccess< typename remove_const< GridImp >::type >;

    template<int,typename, typename>
    friend class P4estGridEntityPointer;

  public:

    // The codimension of this entitypointer wrt the host grid
    enum {CodimInHostGrid = GridImp::HostGridType::dimension - GridImp::dimension};

    // EntityPointer to the equivalent entity in the host grid
    typedef typename GridImp::HostGridType::Traits::template Codim<CodimInHostGrid>::Entity HostGridEntity;

    typedef typename GridImp::template Codim<0>::Geometry Geometry;

    typedef typename GridImp::template Codim<0>::LocalGeometry LocalGeometry;

    //! The Iterator over intersections on this level
    typedef P4estGridLevelIntersectionIterator<GridImp> LevelIntersectionIterator;

    //! The Iterator over intersections on the leaf level
    typedef P4estGridLeafIntersectionIterator<GridImp> LeafIntersectionIterator;

    //! Iterator over descendants of the entity
    typedef P4estGridHierarchicIterator<GridImp> HierarchicIterator;

    //! The type of the EntitySeed interface class
    typedef typename GridImp::template Codim<0>::EntitySeed EntitySeed;



    P4estGridEntity()
      : p4estgrid_(nullptr)
    {}

    P4estGridEntity(const GridImp* p4estgrid, const HostGridEntity& hostEntity)
      : hostEntity_(hostEntity)
      , p4estgrid_(p4estgrid)
    {}

    P4estGridEntity(const GridImp* p4estgrid, HostGridEntity&& hostEntity)
      : hostEntity_(std::move(hostEntity))
      , p4estgrid_(p4estgrid)
    {}

    //! \todo Please doc me !
    P4estGridEntity(const P4estGridEntity& original)
      : hostEntity_(original.hostEntity_)
      , p4estgrid_(original.p4estgrid_)
    {}

    P4estGridEntity(P4estGridEntity&& original)
      : hostEntity_(std::move(original.hostEntity_))
      , p4estgrid_(original.p4estgrid_)
    {}

    //! \todo Please doc me !
    P4estGridEntity& operator=(const P4estGridEntity& original)
    {
      if (this != &original)
      {
        p4estgrid_ = original.p4estgrid_;
        hostEntity_ = original.hostEntity_;
      }
      return *this;
    }

    //! \todo Please doc me !
    P4estGridEntity& operator=(P4estGridEntity&& original)
    {
      if (this != &original)
      {
        p4estgrid_ = original.p4estgrid_;
        hostEntity_ = std::move(original.hostEntity_);
      }
      return *this;
    }

    bool equals(const P4estGridEntity& other) const
    {
      return hostEntity_ == other.hostEntity_;
    }

    //! returns true if father entity exists
    bool hasFather () const {
      return hostEntity_.hasFather();
    }

    //! Create EntitySeed
    EntitySeed seed () const
    {
      return EntitySeed(hostEntity_);
    }

    //! Level of this element
    int level () const
    {
      return hostEntity_.level();
    }


    /** \brief The partition type for parallel computing */
    PartitionType partitionType () const {
      return hostEntity_.partitionType();
    }


    //! Geometry of this entity
    Geometry geometry () const
    {
      return Geometry( hostEntity_.geometry() );
    }


    /** \brief Return the number of subEntities of codimension cc.
     */
    template<int cc>
    int count () const
    {
      return hostEntity_.template count<cc>();
    }


    /** \brief Return the number of subEntities of codimension codim.
     */
    unsigned int subEntities (unsigned int codim) const
    {
      return hostEntity_.subEntities(codim);
    }


    /** \brief Provide access to sub entity i of given codimension. Entities
     *  are numbered 0 ... count<cc>()-1
     */
    template<int cc>
    typename GridImp::template Codim<cc>::Entity subEntity (int i) const {
      return P4estGridEntity<cc,dim,GridImp>(p4estgrid_, hostEntity_.template subEntity<cc>(i));
    }


    //! First level intersection
    P4estGridLevelIntersectionIterator<GridImp> ilevelbegin () const {
      return P4estGridLevelIntersectionIterator<GridImp>(
        p4estgrid_,
        p4estgrid_->getHostGrid().levelGridView(level()).ibegin(hostEntity_));
    }


    //! Reference to one past the last neighbor
    P4estGridLevelIntersectionIterator<GridImp> ilevelend () const {
      return P4estGridLevelIntersectionIterator<GridImp>(
        p4estgrid_,
        p4estgrid_->getHostGrid().levelGridView(level()).iend(hostEntity_));
    }


    //! First leaf intersection
    P4estGridLeafIntersectionIterator<GridImp> ileafbegin () const {
      return P4estGridLeafIntersectionIterator<GridImp>(
        p4estgrid_,
        p4estgrid_->getHostGrid().leafGridView().ibegin(hostEntity_));
    }


    //! Reference to one past the last leaf intersection
    P4estGridLeafIntersectionIterator<GridImp> ileafend () const {
      return P4estGridLeafIntersectionIterator<GridImp>(
        p4estgrid_,
        p4estgrid_->getHostGrid().leafGridView().iend(hostEntity_));
    }


    //! returns true if Entity has NO children
    bool isLeaf() const {
      return hostEntity_.isLeaf();
    }


    //! Inter-level access to father element on coarser grid.
    //! Assumes that meshes are nested.
    typename GridImp::template Codim<0>::Entity father () const {
      return P4estGridEntity(p4estgrid_, hostEntity_.father());
    }


    /** \brief Location of this element relative to the reference element element of the father.
     * This is sufficient to interpolate all dofs in conforming case.
     * Nonconforming may require access to neighbors of father and
     * computations with local coordinates.
     * On the fly case is somewhat inefficient since dofs  are visited several times.
     * If we store interpolation matrices, this is tolerable. We assume that on-the-fly
     * implementation of numerical algorithms is only done for simple discretizations.
     * Assumes that meshes are nested.
     */
    LocalGeometry geometryInFather () const
    {
      return LocalGeometry( hostEntity_.geometryInFather() );
    }


    /** \brief Inter-level access to son elements on higher levels<=maxlevel.
     * This is provided for sparsely stored nested unstructured meshes.
     * Returns iterator to first son.
     */
    P4estGridHierarchicIterator<GridImp> hbegin (int maxLevel) const
    {
      return P4estGridHierarchicIterator<const GridImp>(p4estgrid_, *this, maxLevel);
    }


    //! Returns iterator to one past the last son
    P4estGridHierarchicIterator<GridImp> hend (int maxLevel) const
    {
      return P4estGridHierarchicIterator<const GridImp>(p4estgrid_, *this, maxLevel, true);
    }


    //! \todo Please doc me !
    bool wasRefined () const
    {
      if (p4estgrid_->adaptationStep!=GridImp::adaptDone)
        return false;

      int level = this->level();
      int index = p4estgrid_->levelIndexSet(level).index(*this);
      return p4estgrid_->refinementMark_[level][index];
    }


    //! \todo Please doc me !
    bool mightBeCoarsened () const
    {
      return true;
    }


    // /////////////////////////////////////////
    //   Internal stuff
    // /////////////////////////////////////////


    HostGridEntity hostEntity_;
    const GridImp* p4estgrid_;

  private:

    typedef typename GridImp::ctype ctype;

  }; // end of P4estGridEntity codim = 0


} // namespace Dune


#endif
