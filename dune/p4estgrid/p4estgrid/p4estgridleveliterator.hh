// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRIDLEVELITERATOR_HH
#define DUNE_P4ESTGRIDLEVELITERATOR_HH

#include "p4estgridentitypointer.hh"

/** \file
 * \brief The P4estGridLevelIterator class and its specializations
 */

namespace Dune {




  //**********************************************************************
  //
  // --P4estGridLevelIterator
  /** \brief Iterator over all entities of a given codimension and level of a grid.
   * \ingroup P4estGrid
   */
  template<int codim, PartitionIteratorType pitype, class GridImp>
  class P4estGridLevelIterator :
    public Dune::P4estGridEntityPointer<codim,GridImp,typename GridImp::HostGridType::Traits::template Codim<codim>::template Partition<pitype>::LevelIterator>
  {

    typedef typename GridImp::HostGridType::Traits::template Codim<codim>::template Partition<pitype>::LevelIterator HostGridLevelIterator;
    typedef Dune::P4estGridEntityPointer<codim,GridImp,HostGridLevelIterator> Base;

  public:

    //! Constructor
    explicit P4estGridLevelIterator(const GridImp* p4estgrid, int level)
      : Base(p4estgrid, p4estgrid->hostgrid_->levelGridView(level).template begin<codim,pitype>())
    {}


    /** \brief Constructor which create the end iterator
        \param endDummy      Here only to distinguish it from the other constructor
        \param p4estgrid  pointer to P4estGrid instance
        \param level         grid level on which the iterator shall be created
     */
    explicit P4estGridLevelIterator(const GridImp* p4estgrid, int level, bool endDummy)
      : Base(p4estgrid, p4estgrid->hostgrid_->levelGridView(level).template end<codim,pitype>())
    {}


    //! prefix increment
    void increment() {
      ++this->hostEntityPointer_;
    }

  };


}  // namespace Dune

#endif
