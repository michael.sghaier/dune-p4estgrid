// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRIDLEAFITERATOR_HH
#define DUNE_P4ESTGRIDLEAFITERATOR_HH

#include "p4estgridentitypointer.hh"

/** \file
 * \brief The P4estGridLeafIterator class
 */

namespace Dune {


  /** \brief Iterator over all entities of a given codimension and level of a grid.
   *  \ingroup P4estGrid
   */
  template<int codim, PartitionIteratorType pitype, class GridImp>
  class P4estGridLeafIterator :
    public Dune::P4estGridEntityPointer<codim,GridImp,typename GridImp::HostGridType::template Codim<codim>::template Partition<pitype>::LeafIterator>
  {
  private:

    // LevelIterator to the equivalent entity in the host grid
    typedef typename GridImp::HostGridType::template Codim<codim>::template Partition<pitype>::LeafIterator HostGridLeafIterator;

    typedef Dune::P4estGridEntityPointer<codim,GridImp,HostGridLeafIterator> Base;

  public:

    //! \todo Please doc me !
    explicit P4estGridLeafIterator(const GridImp* p4estgrid) :
      Base(p4estgrid, p4estgrid->hostgrid_->leafGridView().template begin<codim,pitype>())
    {}

    /** \brief Constructor which create the end iterator
     *  \param endDummy      Here only to distinguish it from the other constructor
     *  \param p4estgrid  pointer to grid instance
     */
    explicit P4estGridLeafIterator(const GridImp* p4estgrid, bool endDummy) :
      Base(p4estgrid, p4estgrid->hostgrid_->leafGridView().template end<codim,pitype>())
    {}


    //! prefix increment
    void increment() {
      ++this->hostEntityPointer_;
    }

  };


}  // namespace Dune

#endif
