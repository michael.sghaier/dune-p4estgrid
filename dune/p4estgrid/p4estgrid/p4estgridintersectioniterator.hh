// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_P4ESTGRID_INTERSECTIONITERATOR_HH
#define DUNE_P4ESTGRID_INTERSECTIONITERATOR_HH

#include "p4estgridintersections.hh"
#include "p4estgridentity.hh"

#include <dune/grid/common/intersection.hh>

/** \file
 * \brief The P4estGridLeafIntersectionIterator and P4estGridLevelIntersectionIterator classes
 */

namespace Dune {

  /** \brief Iterator over all element neighbors
   * \ingroup P4estGrid
   * Mesh entities of codimension 0 ("elements") allow to visit all neighbors, where
   * a neighbor is an entity of codimension 0 which has a common entity of codimension 1
   * These neighbors are accessed via a IntersectionIterator. This allows the implement
   * non-matching meshes. The number of neighbors may be different from the number
   * of an element!
   */
  template<class GridImp>
  class P4estGridLeafIntersectionIterator
  {

    enum {dim=GridImp::dimension};

    enum {dimworld=GridImp::dimensionworld};

    // The type used to store coordinates
    typedef typename GridImp::ctype ctype;

    typedef typename GridImp::HostGridType::LeafGridView::IntersectionIterator HostLeafIntersectionIterator;

  public:

    typedef Dune::Intersection<const GridImp, Dune::P4estGridLeafIntersection<GridImp> > Intersection;

    P4estGridLeafIntersectionIterator()
    {}

    P4estGridLeafIntersectionIterator(const GridImp* p4estgrid,
                                         const HostLeafIntersectionIterator& hostIterator)
      : p4estgrid_(p4estgrid)
      , hostIterator_(hostIterator)
    {}

    //! equality
    bool equals(const P4estGridLeafIntersectionIterator& other) const {
      return hostIterator_ == other.hostIterator_;
    }


    //! prefix increment
    void increment() {
      ++hostIterator_;
    }

    //! \brief dereferencing
    Intersection dereference() const {
      return P4estGridLeafIntersection<GridImp>(p4estgrid_,*hostIterator_);
    }

  private:
    //**********************************************************
    //  private data
    //**********************************************************

    const GridImp* p4estgrid_;
    HostLeafIntersectionIterator hostIterator_;
  };




  //! \todo Please doc me !
  template<class GridImp>
  class P4estGridLevelIntersectionIterator
  {
    enum {dim=GridImp::dimension};

    enum {dimworld=GridImp::dimensionworld};

    // The type used to store coordinates
    typedef typename GridImp::ctype ctype;

    typedef typename GridImp::HostGridType::LevelGridView::IntersectionIterator HostLevelIntersectionIterator;

  public:

    typedef Dune::Intersection<const GridImp, Dune::P4estGridLevelIntersection<GridImp> > Intersection;

    P4estGridLevelIntersectionIterator()
    {}

    P4estGridLevelIntersectionIterator(const GridImp* p4estgrid,
                                          const HostLevelIntersectionIterator& hostIterator)
      : p4estgrid_(p4estgrid)
      , hostIterator_(hostIterator)
    {}

    //! equality
    bool equals(const P4estGridLevelIntersectionIterator<GridImp>& other) const {
      return hostIterator_ == other.hostIterator_;
    }


    //! prefix increment
    void increment() {
      ++hostIterator_;
    }

    //! \brief dereferencing
    Intersection dereference() const {
      return P4estGridLevelIntersection<GridImp>(p4estgrid_,*hostIterator_);
    }

  private:


    const GridImp* p4estgrid_;
    HostLevelIntersectionIterator hostIterator_;

  };


}  // namespace Dune

#endif
