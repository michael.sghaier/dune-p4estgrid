// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <dune/grid/yaspgrid.hh>
#include <dune/p4estgrid/p4estgrid.hh>

#include "dune/grid/test/gridcheck.hh"
#include "dune/grid/test/checkintersectionit.hh"

#include <dune/common/parallel/mpihelper.hh> // include mpi helper class

using namespace Dune;

// test P4estGrid for given dimension
template <int dim>
void testDim()
{
  typedef YaspGrid<dim> GridType;
  Dune::array<int,dim> n;
  std::fill(n.begin(), n.end(), 1 << (5 - dim));
  Dune::FieldVector<double,dim> extension(1.0);

  GridType grid(extension,n);

  grid.globalRefine(1);

  P4estGrid<GridType> p4estGrid(grid);

  gridcheck(p4estGrid);
  checkIntersectionIterator(p4estGrid);
}

int main (int argc, char *argv[]) try
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  testDim<1>();
  testDim<2>();
  testDim<3>();

  return 0;
}
// //////////////////////////////////
//   Error handler
// /////////////////////////////////
catch (Exception e)
{
  std::cout << e << std::endl;
  return 1;
}
